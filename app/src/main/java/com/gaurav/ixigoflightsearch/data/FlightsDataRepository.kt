package com.gaurav.ixigoflightsearch.data

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.gaurav.ixigoflightsearch.network.ApiService
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import javax.inject.Inject
import javax.inject.Singleton

/*
* This class is defined as repository to provide data demanded by View Model.
* */

@Singleton
class FlightsDataRepository @Inject constructor(private val apiService: ApiService) {

    /*method to invoke network call to fetch data*/
    fun getFlightsData(): LiveData<FlightsDataResponse> {
        val flightsData: MutableLiveData<FlightsDataResponse> = MutableLiveData()
        apiService.getFlightsData().enqueue(object : Callback<FlightsDataResponse> {
            override fun onResponse(
                call: Call<FlightsDataResponse>,
                response: Response<FlightsDataResponse?>
            ) {
                if (response.isSuccessful) {
                    flightsData.value = response.body()
                }
            }

            override fun onFailure(call: Call<FlightsDataResponse>, t: Throwable?) {
                flightsData.value = null
            }
        })
        return flightsData
    }
}