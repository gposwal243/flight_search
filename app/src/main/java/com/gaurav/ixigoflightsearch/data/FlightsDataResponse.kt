package com.gaurav.ixigoflightsearch.data

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize
import java.util.*
import kotlin.collections.ArrayList

/*
* This is file to contain all the data classes,
* and to perform all the business logic calculations.
* */
@Parcelize
data class FlightsDataResponse(
    @SerializedName("appendix")
    val appendix: Appendix,
    @SerializedName("flights")
    val flights: List<Flight>
) : Parcelable {

    /*This method returns filters and sorts data, as per the user's interaction,
    * It takes providerId and sort Order to filter the data,
    * And returns the list of flights as output data.
    * */
    fun getSegregatedData(id: Int, sortOder: SortOrder): ArrayList<Flight> {
        val dataList: ArrayList<Flight> = ArrayList()
        for (flight in flights) {
            for (fare in flight.fares) {
                if (id == -1 || fare.providerId == id) {
                    flight.providerName = getProviderNameById(fare.providerId)
                    val element = Flight(
                        flight.airlineCode,
                        flight.arrivalTime,
                        flight.`class`,
                        flight.departureTime,
                        flight.destinationCode,
                        flight.fares,
                        flight.originCode,
                        flight.providerName,
                        fare.fare
                    )
                    dataList.add(element)
                }
            }

        }
        return sortListWithOrder(sortOder, dataList)
    }

    private fun sortListWithOrder(
        sortOder: SortOrder,
        dataList: ArrayList<Flight>
    ): ArrayList<Flight> {
        when (sortOder) {
            SortOrder.Earliest -> {
                Collections.sort(dataList, EarliestComparator())
            }
            SortOrder.Cheapest -> {
                Collections.sort(dataList, CheapestComparator())
            }
            SortOrder.Fastest -> {
                Collections.sort(dataList, FastestComparator())
            }
        }
        return dataList
    }

    private fun getProviderNameById(providerId: Int): String {
        return when (providerId) {
            1 -> {
                appendix.providers.`1`
            }
            2 -> {
                appendix.providers.`2`
            }
            3 -> {
                appendix.providers.`3`
            }
            4 -> {
                appendix.providers.`4`
            }

            else -> {
                ""
            }

        }
    }

    fun getProvidersList(): java.util.ArrayList<ProvidersData>? {
        val list: ArrayList<ProvidersData> = ArrayList()
        list.add(ProvidersData(-1, "All booking providers"))
        list.add(ProvidersData(1, appendix.providers.`1`))
        list.add(ProvidersData(2, appendix.providers.`2`))
        list.add(ProvidersData(3, appendix.providers.`3`))
        list.add(ProvidersData(4, appendix.providers.`4`))
        return list
    }
}

@Parcelize
data class Appendix(
    @SerializedName("airlines")
    val airlines: Airlines,
    @SerializedName("airports")
    val airports: Airports,
    @SerializedName("providers")
    val providers: Providers
) : Parcelable

@Parcelize
data class Airlines(
    @SerializedName("6E")
    val `6E`: String,
    @SerializedName("9W")
    val `9W`: String,
    @SerializedName("AI")
    val AI: String,
    @SerializedName("G8")
    val G8: String,
    @SerializedName("SG")
    val SG: String
) : Parcelable

@Parcelize
data class Airports(
    @SerializedName("BOM")
    val BOM: String,
    @SerializedName("DEL")
    val DEL: String
) : Parcelable

@Parcelize
data class Providers(
    @SerializedName("1")
    val `1`: String,
    @SerializedName("2")
    val `2`: String,
    @SerializedName("3")
    val `3`: String,
    @SerializedName("4")
    val `4`: String
) : Parcelable

@Parcelize
data class Flight(
    @SerializedName("airlineCode")
    val airlineCode: String,
    @SerializedName("arrivalTime")
    val arrivalTime: Long,
    @SerializedName("class")
    val `class`: String,
    @SerializedName("departureTime")
    val departureTime: Long,
    @SerializedName("destinationCode")
    val destinationCode: String,
    @SerializedName("fares")
    val fares: List<Fare>,
    @SerializedName("originCode")
    val originCode: String,
    var providerName: String,
    var fare: Int
) : Parcelable

@Parcelize
data class Fare(
    @SerializedName("fare")
    val fare: Int,
    @SerializedName("providerId")
    val providerId: Int
) : Parcelable

@Parcelize
data class ProvidersData(
    val id: Int,
    val providerName: String
) : Parcelable {

    override fun toString(): String {
        return providerName
    }
}

/* Enums defined for sorting order to provide sorting feature to user */
enum class SortOrder {
    Earliest, Cheapest, Fastest
}

/*
* Comparator to sort flight list with departure time.
* */
class EarliestComparator : Comparator<Flight?> {
    override fun compare(left: Flight?, right: Flight?): Int {
        return left!!.departureTime.compareTo(right!!.departureTime)
    }
}

/*
* Comparator to sort flight list with travel fare.
* */
class CheapestComparator : Comparator<Flight?> {
    override fun compare(left: Flight?, right: Flight?): Int {
        return left!!.fare.compareTo(right!!.fare)
    }
}

/*
* Comparator to sort flight list with travel time.
* */
class FastestComparator : Comparator<Flight?> {
    override fun compare(left: Flight?, right: Flight?): Int {
        return (left!!.arrivalTime - left.departureTime).compareTo(right!!.arrivalTime - right.departureTime)
    }
}