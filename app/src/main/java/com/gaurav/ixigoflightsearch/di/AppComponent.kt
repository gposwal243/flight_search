package com.gaurav.ixigoflightsearch.di

import com.gaurav.ixigoflightsearch.ui.MainActivity
import dagger.Component
import javax.inject.Singleton

/*
* Dagger Component for the two modules i.e. NetworkModule & ViewModelModule to provide dependencies
* required in the application.
* */
@Singleton
@Component(modules = [NetworkModule::class, ViewModelModule::class])
interface AppComponent {
    fun inject(activity: MainActivity)


}