package com.gaurav.ixigoflightsearch.network

import com.gaurav.ixigoflightsearch.data.FlightsDataResponse
import retrofit2.Call
import retrofit2.http.GET

/*
* This is the network service interface to be used to declare the network api calls
* to be called with retrofit client.
* */
interface ApiService {
    companion object {
        const val BASE_URL = "http://www.mocky.io/"
        const val API_VERSION = "v2/"
        const val API_KEY = "5979c6731100001e039edcb3"
    }

    /*
    * Network call method to fetch the flights data
    * */
    @GET(API_VERSION + API_KEY)
    fun getFlightsData(): Call<FlightsDataResponse>

//    Define more method here for other apis

}