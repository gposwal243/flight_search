package com.gaurav.ixigoflightsearch.base

import android.app.Application
import com.gaurav.ixigoflightsearch.di.AppComponent
import com.gaurav.ixigoflightsearch.di.DaggerAppComponent


class MyApplication : Application() {

    /*Initialize dagger component*/
    val appComponent: AppComponent = DaggerAppComponent.create()

}