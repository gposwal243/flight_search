package com.gaurav.ixigoflightsearch.ui.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.gaurav.ixigoflightsearch.R
import com.gaurav.ixigoflightsearch.data.Appendix
import com.gaurav.ixigoflightsearch.data.Flight
import com.gaurav.ixigoflightsearch.utils.Utils
import kotlinx.android.synthetic.main.row_item_flights.view.*

/*
* This class is the Recycler View Adapter for the flights listing.
* */
class FlightsAdapter(
    private val _mContext: Context,
    private val _dataList: ArrayList<Flight>,
    private var dataAppendix: Appendix?
) :
    RecyclerView.Adapter<FlightsAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            LayoutInflater.from(_mContext).inflate(
                R.layout.row_item_flights,
                parent,
                false
            )
        )
    }

    override fun getItemCount(): Int {
        return _dataList.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val listItem = _dataList[position]

        /*Calling method in view holder class to populate the data for the list items*/
        holder.populateData(listItem, dataAppendix)

    }

    /*
    * View Holder class for an item in the list.
    * */
    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        private var view = itemView

        fun populateData(
            listItem: Flight,
            dataAppendix: Appendix?
        ) {
            view.tvAirlineName.text = listItem.providerName
            view.tvFlightPrice.text = String.format("%s %s", Utils.INDIAN_RUPEE, listItem.fare)
            view.tvClass.text = listItem.`class`

            /*Methods are written in Util class mapping and showing values after mapping from appendix*/
            Utils.setSourceDestination(listItem.originCode, dataAppendix, view.tvDeparturePlace)
            Utils.setSourceDestination(listItem.destinationCode, dataAppendix, view.tvArrivalPlace)
            Utils.setAirlines(
                listItem.airlineCode,
                view.ivProvider,
                dataAppendix,
                view.tvAirlineName
            )
            Utils.setAirlines(
                listItem.airlineCode,
                view.ivProvider,
                dataAppendix,
                view.tvAirlineName
            )

            /*Methods are written in Util class for date/time parsing from time stamp*/
            Utils.showConvertedTime(listItem.departureTime, view.tvTimeArrival)
            Utils.showConvertedTime(listItem.arrivalTime, view.tvTimeDeparture)
            Utils.showConvertedDate(listItem.departureTime, view.tvDepartureDate)
            Utils.showConvertedDate(listItem.arrivalTime, view.tvArrivalDate)
            Utils.showTimeTaken(listItem.arrivalTime, listItem.departureTime, view.tvTotalTime)
        }
    }


    fun updateList(
        newData: ArrayList<Flight>,
        dataAppendix: Appendix?
    ) {
        this.dataAppendix = dataAppendix
        _dataList.clear()
        _dataList.addAll(newData)
        notifyDataSetChanged()
    }
}