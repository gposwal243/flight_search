package com.gaurav.ixigoflightsearch.ui

import android.os.Bundle
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.gaurav.ixigoflightsearch.R
import com.gaurav.ixigoflightsearch.base.MyApplication
import com.gaurav.ixigoflightsearch.data.*
import com.gaurav.ixigoflightsearch.ui.adapter.FlightsAdapter
import com.gaurav.ixigoflightsearch.utils.Utils
import com.gaurav.ixigoflightsearch.viewmodel.FlightsViewModel
import com.google.android.material.tabs.TabLayout
import com.google.android.material.tabs.TabLayout.OnTabSelectedListener
import kotlinx.android.synthetic.main.activity_main.*
import javax.inject.Inject

/*
* This class is the main/parent activity having all ui components over it.
* ViewModelProvider.Factory instance in injected via dagger framework to get the viewmodel instance.
* */

class MainActivity : AppCompatActivity() {
    lateinit var flightsViewModel: FlightsViewModel
    /**
     * Injecting via dagger
     * Profit is that we don't need to create every different viewModelFactory class with different
     * params.
     */
    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    /*data list which is getting passed to adapter finally after getting it form ViewModel.*/
    private val dataList: ArrayList<Flight> = ArrayList()

    /*data appendix object which is used for mapping values from appendix to show value on the
    *views.
    */
    private var dataAppendix: Appendix? = null

    /*list of all booking providers getting from view model to inflate the spinner in the
    *toolbar.
    * */
    private val providersList: ArrayList<ProvidersData> = ArrayList()

    /*Field for Recycler view adapter to show flight list.*/
    private lateinit var mFlightsAdapter: FlightsAdapter

    /*Field for Spinner view adapter */
    private lateinit var spinnerAdapter: ArrayAdapter<ProvidersData>

    override fun onCreate(savedInstanceState: Bundle?) {
        (applicationContext as MyApplication).appComponent.inject(this)
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        /*Getting viewmodel instance with ViewModelProvider */
        flightsViewModel = ViewModelProvider(this, viewModelFactory)[FlightsViewModel::class.java]

        /*Checking data for null,
        * This is handled to avoid data re-fetch on orientation changes.
        * If data is not null, then we don't need to make network call, if
        * requires we can make it with swipe refresh view.
        *  */
        if (flightsViewModel.getCurrentFlightsData() == null)
            getFlightsData(false)

        initViews()
    }

    private fun initViews() {
        setTabs()
        setToolBar()
        setSpinner()
        setRecyclerView()
        updateViews()
        swipeRefresh.setOnRefreshListener {
            getFlightsData(true)
        }
    }

    private fun setToolBar() {
        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowHomeEnabled(true)
        toolbar.setNavigationOnClickListener {
            finish()
        }
    }

    private fun setSpinner() {
        // Initializing an ArrayAdapter
        spinnerAdapter = ArrayAdapter(
            this, // Context
            R.layout.spinner_item, // Layout
            providersList // Array
        )
        // Set the drop down view resource
        spinnerAdapter.setDropDownViewResource(android.R.layout.simple_dropdown_item_1line)

        // Finally, data bind the spinner object with adapter
        spnFlights.adapter = spinnerAdapter

        // Set an on item selected listener for spinner object
        spnFlights.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(
                parent: AdapterView<*>,
                view: View,
                position: Int,
                id: Long
            ) {
                // Display the selected item text on text view
                flightsViewModel.setCurrentProvider(providersList[position].id)
            }

            override fun onNothingSelected(parent: AdapterView<*>) {
                // Another interface callback
            }
        }
    }

    /*
    * This method is written to subscribe listeners to update list data after the user's
    * interaction with the app i.e. on changing value in spinner or on selecting tab to change
    * sorting order.
    * */
    private fun updateViews() {

        flightsViewModel.getCurrentProvider().observe(this, Observer<Int> {
            flightsViewModel.updateUiWithFlightsList(id = it)
        })

        flightsViewModel.getSortOrder().observe(this, Observer<SortOrder> {
            flightsViewModel.updateUiWithFlightsList(sortOrder = it)
        })


        /*Subscription to data observation in data list, if there is any change it will refresh list */
        flightsViewModel.getFlightsListData().observe(
            this, Observer<ArrayList<Flight>> {
                if (it != null) {
                    updateListData(it)
                }
            })

        /*To refresh list  providers when data got re-fetched.
        * */
        flightsViewModel.getProvidersListData().observe(
            this, Observer<ArrayList<ProvidersData>> {
                if (it != null) {
                    updateProvidersList(it)
                }
            })
    }

    private fun updateListData(dataList: java.util.ArrayList<Flight>) {
        mFlightsAdapter.updateList(dataList, dataAppendix)
    }

    private fun updateProvidersList(providers: ArrayList<ProvidersData>) {
        providersList.clear()
        providersList.addAll(providers)
        spinnerAdapter.notifyDataSetChanged()
    }

    private fun setTabs() {

        //To manage the consistency in tab selection for sorting even after the orientation changes.
        tabLayout.selectTab(
            tabLayout.getTabAt(
                flightsViewModel.getSortOrder().value?.ordinal ?: 0
            )
        )
        tabLayout.setTabTextColors(
            ContextCompat.getColor(this, R.color.colorLightBlack),
            ContextCompat.getColor(this, R.color.colorWhite)
        )

        // This is the subcription to listen whether the sorting order is changed by user.
        tabLayout.addOnTabSelectedListener(object : OnTabSelectedListener {
            override fun onTabSelected(tab: TabLayout.Tab) {
                when (tab.position) {
                    0 -> {
                        flightsViewModel.setSortOrder(SortOrder.Earliest)
                    }
                    1 -> {
                        flightsViewModel.setSortOrder(SortOrder.Cheapest)
                    }
                    2 -> {
                        flightsViewModel.setSortOrder(SortOrder.Fastest)
                    }
                }
            }

            override fun onTabUnselected(tab: TabLayout.Tab) {}
            override fun onTabReselected(tab: TabLayout.Tab) {}
        })
    }

    private fun setRecyclerView() {
        rvFlights.layoutManager = LinearLayoutManager(this)
        mFlightsAdapter = FlightsAdapter(this, dataList, dataAppendix)
        rvFlights.adapter = mFlightsAdapter
        rvFlights.isNestedScrollingEnabled = true
    }


    //    Method to make the network call via ViewModel
    private fun getFlightsData(isRefreshed: Boolean) {
        if (Utils.isOnline(this)) {

            swipeRefresh.isRefreshing = true
            flightsViewModel.getFlightsData().observe(
                this, Observer<FlightsDataResponse> {
                    swipeRefresh.isRefreshing = false

                    if (it != null) {

                        showErrorMsg(false)
                        flightsViewModel.setFlightsData(it)
                        dataAppendix = it.appendix
                        // calling this method to inflate the whole list without any selection
                        if (!isRefreshed)
                            flightsViewModel.setCurrentProvider(-1)
                    } else {
                        tvNoData.text = getString(R.string.no_data_found)
                        showErrorMsg(true)
                    }
                })
        } else {
            swipeRefresh.isRefreshing = false
            tvNoData.text = getString(R.string.check_connection)
            showErrorMsg(true)
        }
    }

    /*This is the method written to manage the views on occurrence of errors
    *due to internet connectivity or network call failure
    * */
    private fun showErrorMsg(flag: Boolean) {
        if (flag) {
            rvFlights.visibility = View.GONE
            tvNoData.visibility = View.VISIBLE
        } else {
            rvFlights.visibility = View.VISIBLE
            tvNoData.visibility = View.GONE
        }


    }

}






