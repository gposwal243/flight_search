package com.gaurav.ixigoflightsearch.utils

import android.content.Context
import android.net.ConnectivityManager
import android.net.NetworkCapabilities
import android.os.Build
import android.widget.ImageView
import android.widget.TextView
import com.gaurav.ixigoflightsearch.R
import com.gaurav.ixigoflightsearch.data.Appendix
import java.text.SimpleDateFormat
import java.util.*


object Utils {

    const val INDIAN_RUPEE = "₹"

    /* This is the method responsible for checking whether
    *there is proper internet connectivity or not
    * */
    fun isOnline(context: Context): Boolean {
        val cm = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            val n = cm.activeNetwork
            if (n != null) {
                val nc = cm.getNetworkCapabilities(n)
                //It will check for both wifi and cellular network
                return nc!!.hasTransport(NetworkCapabilities.TRANSPORT_CELLULAR) || nc.hasTransport(
                    NetworkCapabilities.TRANSPORT_WIFI
                )
            }
            return false
        } else {
            val netInfo = cm.activeNetworkInfo
            return netInfo != null && netInfo.isConnectedOrConnecting
        }
    }

    /*Method to show airline names from appendix code*/
    fun setAirlines(
        airlinesCode: String,
        imageView: ImageView,
        dataAppendix: Appendix?,
        tvAirlineName: TextView
    ) {
        when (airlinesCode) {
            "SG" -> {
                tvAirlineName.text = dataAppendix?.airlines?.SG
                imageView.setImageResource(R.mipmap.ic_spicejet)
            }
            "AI" -> {
                tvAirlineName.text = dataAppendix?.airlines?.AI
                imageView.setImageResource(R.mipmap.ic_air_india)
            }
            "G8" -> {
                tvAirlineName.text = dataAppendix?.airlines?.G8
                imageView.setImageResource(R.mipmap.ic_go_air)
            }
            "9W" -> {
                tvAirlineName.text = dataAppendix?.airlines?.`9W`
                imageView.setImageResource(R.mipmap.ic_jet_airways)
            }
            "6E" -> {
                tvAirlineName.text = dataAppendix?.airlines?.`6E`
                imageView.setImageResource(R.mipmap.ic_indigo)
            }
            else -> {
                tvAirlineName.text = dataAppendix?.airlines?.SG
                imageView.setImageResource(R.mipmap.ic_spicejet)
            }
        }
    }

    /*Method to show destination names from appendix code*/
    fun setSourceDestination(
        code: String,
        dataAppendix: Appendix?,
        tvPlace: TextView
    ) {
        when (code) {
            "BOM" -> {
                tvPlace.text = dataAppendix?.airports?.BOM
            }
            "DEL" -> {
                tvPlace.text = dataAppendix?.airports?.DEL
            }
        }
    }

    /*Method to show times after parsing the timestamp*/
    fun showConvertedTime(time: Long, tvTime: TextView) {
        try {
            val sdf = SimpleDateFormat("HH:mm", Locale.getDefault())
            sdf.timeZone = TimeZone.getTimeZone("UTC")
            val date = Date(time)
            tvTime.text = sdf.format(date.time)
        } catch (e: java.text.ParseException) {
            e.printStackTrace()
        }
    }

    /*Method to show dates after parsing the timestamp*/
    fun showConvertedDate(time: Long, tvTime: TextView) {
        try {
            val sdf = SimpleDateFormat("dd MMM", Locale.getDefault())
            sdf.timeZone = TimeZone.getTimeZone("UTC")
            val date = Date(time)
            tvTime.text = sdf.format(date.time)
        } catch (e: java.text.ParseException) {
            e.printStackTrace()
        }
    }

    /*Method to show total time taken in the journey after parsing the timestamps*/
    fun showTimeTaken(arrivalTime: Long, departureTime: Long, tvTime: TextView) {
        try {
            val sdf = SimpleDateFormat("HH:mm", Locale.getDefault())
            sdf.timeZone = TimeZone.getTimeZone("UTC")
            val arrivalDate = Date(arrivalTime)
            val departureDate = Date(departureTime)
            val millis = (arrivalDate.time - departureDate.time)
            val hours: Long = millis / (1000 * 60 * 60)
            val mins: Long = millis / (1000 * 60) % 60
            tvTime.text = String.format("%sh %sm", hours, mins)
        } catch (e: java.text.ParseException) {
            e.printStackTrace()
        }
    }

}

