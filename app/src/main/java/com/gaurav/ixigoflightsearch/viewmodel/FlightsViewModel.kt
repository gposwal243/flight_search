package com.gaurav.ixigoflightsearch.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.gaurav.ixigoflightsearch.data.*
import javax.inject.Inject

/*
* This is the ViewModel for the Main Activity, which is responsible to provide all the data to views
* and also for actions to be responded after user's interaction e.g network call.
* */

class FlightsViewModel @Inject constructor(private val flightsRepo: FlightsDataRepository) :
    ViewModel() {
    /*
    * This is the data model object to be maintained or to be persisted in the viewmodel,
    * it got updated even after network call,
    * and all data needed in the views is extracted from this instance.
    * */
    private var flightsData: FlightsDataResponse? = null

    /*Live data for currently selected provider in spinner,
    * It notifies that user has changed the provider after which we're
    * updating data in the views.
    * */
    private var currentProvider: MutableLiveData<Int> = MutableLiveData()

    /*
    * This is the live data for the list of flights to be shown in the list currently on the basis
    * of the filters user has applied
    * */
    private var flightsListData: MutableLiveData<ArrayList<Flight>> = MutableLiveData()

    /*This is the live data for the list of providers to make the spinner adapter updated always
    *when there is any change in data.
    * */
    private var providersListData: MutableLiveData<ArrayList<ProvidersData>> = MutableLiveData()

    /*This is the live data for the enum of currently applied sorted order,
    * whenever there will be any change in data,
    * we'll get notified and we can update the list of data accordingly*/
    private var sortOrderLiveData: MutableLiveData<SortOrder> = MutableLiveData()

    fun getFlightsData(): LiveData<FlightsDataResponse> {
        return flightsRepo.getFlightsData()
    }

    fun getCurrentProvider(): LiveData<Int> {
        return currentProvider
    }

    fun setCurrentProvider(providerId: Int) {
        currentProvider.value = providerId
    }

    fun getSortOrder(): LiveData<SortOrder> {
        return sortOrderLiveData
    }

    fun setSortOrder(sortOrder: SortOrder) {
        sortOrderLiveData.value = sortOrder
    }

    fun getFlightsListData(): LiveData<ArrayList<Flight>> {
        return flightsListData
    }

    /*This method is written to update the live data of flight list after the segregation required,
    * on the basis of filters applied by user in the form of provider and  sort order*/
    fun updateUiWithFlightsList(
        id: Int = currentProvider.value ?: -1,
        sortOrder: SortOrder = sortOrderLiveData.value ?: SortOrder.Earliest
    ) {
        flightsListData.value = flightsData?.getSegregatedData(id, sortOrder)
    }

    fun getProvidersListData(): LiveData<ArrayList<ProvidersData>> {
        return providersListData
    }

    private fun updateProvidersList() {
        providersListData.value = flightsData?.getProvidersList()
    }

    fun setFlightsData(flightsDataResponse: FlightsDataResponse?) {
        flightsData = flightsDataResponse
        updateProvidersList()
    }

    fun getCurrentFlightsData(): FlightsDataResponse? {
        return flightsData
    }
}


